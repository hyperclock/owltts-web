# OwlTTS Web
This part of the OwlTTS project is to be a web interface. We may use some of the Free TTS websites as an example for usage on <a href="https://owltts.org" target="_blank">owl.org</a>

## Documentation
Some docs may be kept in [our WIKI](../../wikis/home)

## Support
Bugs, issues, suggestions, requests, feedback etc. can be placed into [our ISSUES area](../../issues)

## Roadmap
Follow [our Milestones](../../milestones) and see where development is heading.

## Contributing
Contributions are welcome. For now just fork the project and ater your contribution is ready, ask to merge it into our project.

## License
Open source licensed. See [LICENSE](LICENSE) for details.

## Project status
In active development.
